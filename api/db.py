import os
from psycopg_pool import ConnectionPool

user = os.environ["PGUSER"]
password = os.environ["PGPASSWORD"]
db = os.environ["PGDATABASE"]
host = os.environ["PGHOST"]

url = f'postgresql://{user}:{password}@{host}/{db}'
pool = ConnectionPool(conninfo=url)

class CategoryQueries:
    def categories_list(self, limit=100, offset=0):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    SELECT id, title, canon
                    FROM categories
                    ORDER BY id
                    LIMIT %s
                    OFFSET %s
                """, [limit, offset])

                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)

                return results

    def create_category(self, category_in):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    INSERT INTO categories (title, canon)
                    VALUES (%s, false)
                    RETURNING id, title, canon
                """, [category_in.title])

                record = None
                row = cur.fetchone()
                if row:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]

                return record

    def update_category(self, category_in, category_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    UPDATE categories
                    SET title = %s
                    WHERE id = %s
                    RETURNING id, title, canon
                """, [category_in.title, category_id])

                record = None
                row = cur.fetchone()
                if row:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]

                return record

    def delete_category(self, category_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    SELECT * FROM categories
                    WHERE id = %s
                """, [category_id])

                row = cur.fetchone()

                cur.execute(f"""
                    DELETE FROM categories
                    WHERE id = %s
                """, [category_id])

                if row:
                    return True
                else:
                    return False
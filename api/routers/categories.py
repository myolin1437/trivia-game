from fastapi import APIRouter, Depends, Response
from pydantic import BaseModel

from db import CategoryQueries

router = APIRouter()

class CategoryIn(BaseModel):
    title: str


class CategoryOut(BaseModel):
    id: int
    title: str
    canon: bool


class CategoriesOut(BaseModel):
    categories: list[CategoryOut]


@router.get("/api/categories", response_model=CategoriesOut)
def categories_list(
    queries: CategoryQueries = Depends(), 
    page: int = 0
    ):
    return {
        "categories": queries.categories_list(offset=page*100),
    }


@router.post("/api/categories", response_model=CategoryOut)
def create_category(
    category_in: CategoryIn, 
    queries: CategoryQueries = Depends()
    ):
    return queries.create_category(category_in)


@router.put("/api/categories/{category_id}", response_model=CategoryOut)
def update_category(
    category_id: int,
    category_in: CategoryIn,
    response: Response,
    queries: CategoryQueries = Depends()
    ):
    record = queries.update_category(category_in, category_id)
    if record:
        return record
    else:
        response.status_code = 404


@router.delete("/api/categories/{category_id}")
def delete_category(
    category_id: int, 
    queries: CategoryQueries = Depends()
    ):
    return queries.delete_category(category_id)
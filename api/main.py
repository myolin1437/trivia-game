from fastapi import FastAPI, Depends

from routers import categories

app = FastAPI()

app.include_router(categories.router)

